**Setup**
- Download xCode 9
- clone the project to your machine. 
- Import the poject and build it to your target device

### What is this repository for? ###

**SC-Calculator** makes cashier's life easier. 
It's designed to help kiosk-cashier's  @schwarzwald-stadion in Freiburg im Breisgau.

**Get Matchday statistics** sold units, revenue, best seller and average order total
**Let Sc Calculator do the math** so that you can focus on 🍻


![Alt text](https://i.imgur.com/rWg7i2Q.png?1 "Calculation scenario")  ------  ![Alt text](https://i.imgur.com/NXFopHe.png?1 "Statistics scenario")



Version: 1.1


### Who do I talk to? ###

www.patrice.codes